package com.ra.jwt;

import java.io.IOException;

import com.ra.jwt.services.JwtService;
import com.ra.user.services.UserInfoService;
import jakarta.servlet.http.Cookie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * The JwtAuthFilter class performs authentication and processing of requests containing JWT tokens.
 * It is a Spring Security filter used to authenticate and authorize users based on tokens.
 */
@Component
public class JwtAuthFilter extends OncePerRequestFilter {

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserInfoService userDetailsService;

    /**
     * Method to perform authentication and request processing.
     * @param request The HttpServletRequest being processed.
     * @param response The HttpServletResponse to return the result.
     * @param filterChain The FilterChain for passing the request along the chain.
     * @throws ServletException If a servlet exception occurs.
     * @throws IOException If an I/O exception occurs.
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        // Get the token from the cookie
        String token = getTokenFromCookie(request, response);

        // If token is null or empty, allow the request to continue its navigation
        if (token == null || token.isEmpty()) {
            filterChain.doFilter(request, response);
            return;
        }

        // Extract the username from the token
        String userName = getUserNameFromToken(token);

        // If username is null or token is not valid, allow the request to continue its navigation
        if (userName == null) {
            filterChain.doFilter(request, response);
            return;
        }

        // Attempt to load the user details and validate the token
        try {
            UserDetails userDetails = userDetailsService.loadUserByUsername(userName);

            if (!jwtService.validateToken(token, userDetails)) {
                filterChain.doFilter(request, response);
                return;
            }

            // If authentication object doesn't exist, create a new one and set it into SecurityContextHolder
            if (SecurityContextHolder.getContext().getAuthentication() == null) {
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        } catch (UsernameNotFoundException ex) {
            // If user not found, just continue the filter chain
            filterChain.doFilter(request, response);
            return;
        }

        // Allow the request to continue its navigation
        filterChain.doFilter(request, response);
    }

    private String getTokenFromCookie(HttpServletRequest request, HttpServletResponse response) {
        String tokenCookieName = "token";
        String token = null;
        String username = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (tokenCookieName.equals(cookie.getName())) {
                    token = cookie.getValue().toString();
                    break;
                }
            }
        }
        return token;
    }

    private String getUserNameFromToken(String token) {

        String requestTokenHeader = "Bearer " + token;
        String username = null;
        String jwtToken = null;

        if (token != null && !token.isEmpty() && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = jwtService.extractUsername(jwtToken);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return username;
    }

}