package com.ra.config;

import com.ra.user.entities.Employee;
import com.ra.user.entities.UserInfo;
import com.ra.user.services.EmployeeService;
import com.ra.user.services.UserInfoService;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

@Component
@AllArgsConstructor
public class SecurityUtil {
    private final UserInfoService userInfoService;

    private final EmployeeService employeeService;

    // Static method to retrieve the username of the currently authenticated user
    public static String getSessionUser() {
        // Retrieve the Authentication object from the SecurityContext
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        // Check if authentication is not null and is not an instance of AnonymousAuthenticationToken
        if (authentication != null && !(authentication instanceof AnonymousAuthenticationToken)) {
            // Return the username of the authenticated user
            return authentication.getName();
        }

        // If authentication is null or is an instance of AnonymousAuthenticationToken, return null
        return null;
    }

    // Get user information
    public void getUserInfo(Model model) {
        UserInfo user = new UserInfo();
        String email = SecurityUtil.getSessionUser();
        if(email != null)
            user = userInfoService.findFirstByEmail(email);

        Employee employee = new Employee();

        if (user != null)
            employee = employeeService.findFirstByUserInfoId(user.getId());

        model.addAttribute("user", employee);
        model.addAttribute("employee", employee);
    }
}
