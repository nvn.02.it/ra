package com.ra.method.dtos;

import com.ra.classroom.entities.MethodCourse;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MethodDTO {
    public Long id;
    public String name;
    public List<MethodCourse> methodCourse;
}
