package com.ra.method.services;

import com.ra.method.dtos.MethodDTO;
import com.ra.method.entities.Method;
import com.ra.method.repositories.MethodRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MethodService {

    private final MethodRepository methodRepository;
    private final ModelMapper modelMapper;

    /**
     * Retrieves all methods.
     *
     * @return a list of MethodDTO objects.
     */
    public List<MethodDTO> getAll(){
        List<Method> methods =methodRepository.findAll();
        List<MethodDTO> methodDTOS = methods.stream()
                .map(this::convertToDTO).collect(Collectors.toList());
        return methodDTOS ;
    }

    /**
     * Converts a Method entity to a MethodDTO.
     *
     * @param method the Method entity to convert.
     * @return the converted MethodDTO object.
     */
    private MethodDTO convertToDTO(Method method) {
        MethodDTO methodDTO = modelMapper.map(method, MethodDTO.class);
        return methodDTO;
    }
}
