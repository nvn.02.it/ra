package com.ra.classroom.services;

import com.ra.classroom.dtos.ClassStatusDTO;
import com.ra.classroom.entities.ClassStatus;
import com.ra.classroom.repositories.ClassStatusRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ClassStatusService {

    private final ClassStatusRepository classStatusRepository;
    private final ModelMapper modelMapper;

    /**
     * Retrieves all class statuses.
     *
     * @return a list of ClassStatusDTO objects.
     */
    public List<ClassStatusDTO> getAll(){
        List<ClassStatus> classStatuses =classStatusRepository.findAll();
        List<ClassStatusDTO> classStatusDTOS = classStatuses.stream()
                .map(this::convertToDTO).collect(Collectors.toList());
        return classStatusDTOS ;
    }

    /**
     * Converts a ClassStatus entity to a ClassStatusDTO.
     *
     * @param classStatus the ClassStatus entity to convert.
     * @return the converted ClassStatusDTO object.
     */
    private ClassStatusDTO convertToDTO(ClassStatus classStatus) {
        ClassStatusDTO classStatusDTO = modelMapper.map(classStatus, ClassStatusDTO.class);
        return classStatusDTO;
    }
}
