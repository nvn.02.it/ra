package com.ra.classroom.services;

import com.ra.classroom.dtos.ClassDTO;
import com.ra.classroom.entities.Class;
import com.ra.classroom.repositories.ClassRepository;
import com.ra.exceptions.ClassAlreadyExistsException;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class ClassService {

    private static final int DEFAULT_PAGE_SIZE = 10;
    private final ClassRepository classRepository;
    private final ModelMapper modelMapper;

    /**
     * Retrieves all classes with pagination.
     *
     * @param page the page number to retrieve.
     * @return a page of ClassDTO objects.
     */
    public Page<ClassDTO> getAllClass(Integer page) {
        Pageable pageable = PageRequest.of(page - 1, DEFAULT_PAGE_SIZE);
        Page<Class> classPage = classRepository.findAll(pageable);

        List<ClassDTO> classDTO = classPage.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());

        return new PageImpl<>(classDTO, pageable, classPage.getTotalElements());
    }

    /**
     * Retrieves classes by name or class code with pagination.
     *
     * @param page       the page number to retrieve.
     * @param nameOrCode the name or class code to search for.
     * @return a page of ClassDTO objects.
     */
    public Page<ClassDTO> getByNameOrClassCode(Integer page, String nameOrCode) {
        Pageable pageable = PageRequest.of(page - 1, DEFAULT_PAGE_SIZE);
        Page<Class> classPage = classRepository.findClassesByCourseNameOrClassCode(nameOrCode, nameOrCode, pageable);

        List<ClassDTO> classDTOs = classPage.getContent().stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());

        return new PageImpl<>(classDTOs, pageable, classPage.getTotalElements());
    }

    /**
     * Retrieves a class by its ID.
     *
     * @param id the ID of the class.
     * @return the ClassDTO object.
     */
    public ClassDTO getClassById(Long id) {
        Optional<Class> classRoom = classRepository.findById(id);
        ClassDTO classDTO = modelMapper.map(classRoom.get(), ClassDTO.class);
        return classDTO;
    }

    /**
     * Updates a class.
     *
     * @param classDTO the ClassDTO object containing updated information.
     */
    public void update(ClassDTO classDTO) {

        Optional<Class> classOptional = classRepository.findById(classDTO.getId());
        if (classOptional.isPresent()) {

            Class classRoom = classOptional.get();
            if (!classRoom.getClassCode().equals(classDTO.getClassCode())
                    && classRepository.existsByClassCode(classDTO.getClassCode())) {
                throw new ClassAlreadyExistsException(classDTO.getClassCode());
            }
            Class classRoomMapper = modelMapper.map(classDTO, Class.class);
            classRepository.save(classRoomMapper);
        }

    }

    /**
     * Creates a new class.
     *
     * @param classDTO the ClassDTO object containing class information.
     */
    public void create(ClassDTO classDTO) {
        if (classRepository.existsByClassCode(classDTO.getClassCode())) {
            throw new ClassAlreadyExistsException(classDTO.getClassCode());
        }
        Class classRoom = modelMapper.map(classDTO, Class.class);
        classRepository.save(classRoom);
    }

    /**
     * Deletes a class by its ID.
     *
     * @param id the ID of the class to delete.
     */
    public void deleteById(Long id) {
        classRepository.deleteById(id);
    }

    /**
     * Converts a Class entity to a ClassDTO.
     *
     * @param classRoom the Class entity to convert.
     * @return the converted ClassDTO object.
     */
    private ClassDTO convertToDTO(Class classRoom) {
        ClassDTO classDTO = modelMapper.map(classRoom, ClassDTO.class);
        return classDTO;
    }


}
