package com.ra.classroom.dtos;

import com.ra.classroom.entities.Class;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ClassStatusDTO {
    public Long id;
    public String name;
    public List<Class> classes;
}
