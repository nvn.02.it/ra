package com.ra.classroom.dtos;

import com.ra.classroom.entities.ClassStatus;
import com.ra.classroom.entities.MethodCourse;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClassDTO {

    public Long id;
//    @NotEmpty(message = "{validation.class.classCode.notEmpty}")
    public String classCode;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public Date startDate;
    public String classSchedule;
    public String startHour;
    public String endHour;
    public Integer numberOfStudents;
    public ClassStatus classStatus;
    public MethodCourse methodCourse;
}
