package com.ra.classroom.repositories;

import com.ra.classroom.entities.ClassStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassStatusRepository extends JpaRepository<ClassStatus,Long> {
}
