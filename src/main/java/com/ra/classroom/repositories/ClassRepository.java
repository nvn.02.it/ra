package com.ra.classroom.repositories;

import com.ra.classroom.entities.Class;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassRepository extends JpaRepository<Class,Long> {
    boolean existsByClassCode(String classCode);

    @Query("SELECT c FROM Class c WHERE c.methodCourse.course.name LIKE %:courseName% OR c.classCode LIKE %:classCode%")
    Page<Class> findClassesByCourseNameOrClassCode(@Param("courseName") String courseName,
                                                   @Param("classCode") String classCode, Pageable pageable);
}
