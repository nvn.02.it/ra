package com.ra.classroom.repositories;

import com.ra.classroom.entities.MethodCourse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MethodCourseRepository extends JpaRepository<MethodCourse,Long> {
}
