package com.ra.classroom.controllers;

import com.ra.classroom.dtos.ClassDTO;
import com.ra.classroom.entities.ClassStatus;
import com.ra.classroom.services.ClassService;
import com.ra.classroom.services.ClassStatusService;
import com.ra.course.services.CourseService;
import com.ra.method.services.MethodService;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@AllArgsConstructor
@RequestMapping("/class")
public class ClassController {

    private final String LIST_CLASS_VIEW = "list-class";
    private final String ADD_CLASS_VIEW = "add-class";
    private final String UPDATE_CLASS_VIEW = "update-class";
    private final Long ID_STATUS_CLASS_DEFAULT =1L;
    private final ClassService classService;
    private final MethodService methodService;
    private final CourseService courseService;
    private final ClassStatusService classStatusService;

    @GetMapping("")
    public String showAllClass(@RequestParam(value = "page", defaultValue = ("1")) @Min(1) Integer page
            , Model model) {
        Page<ClassDTO> classDTOS = classService.getAllClass(page);
        model.addAttribute("listClass", classDTOS);
        return LIST_CLASS_VIEW;
    }

    @GetMapping("/search")
    public String findNameOrId(@RequestParam("name") String name, Model model
            , @RequestParam(value = "page", defaultValue = ("1")) @Min(1) Integer page) {
        model.addAttribute("name", name);
        Page<ClassDTO> categories = classService.getByNameOrClassCode(page,name);
        model.addAttribute("listClass", categories);

        return LIST_CLASS_VIEW;
    }

    @GetMapping("/delete/{id}")
    public String deleteClass(@PathVariable("id") Long id) {
        try {
            classService.deleteById(id);
            return "redirect:/class?delete-success";
        }catch (Exception e){
            return "redirect:/class?delete-error";
        }

    }

    @GetMapping("/add")
    public String showAddClass(Model model) {
        model.addAttribute("methods", methodService.getAll());
        model.addAttribute("courses", courseService.getAll());
        ClassDTO classDTO = new ClassDTO();
        model.addAttribute("class", classDTO);
        return ADD_CLASS_VIEW;
    }

    @PostMapping("/create")
    public String createClass(@ModelAttribute("class") ClassDTO classDTO,
                            @RequestParam(name = "isContinue", required = false) String isContinue) {
        ClassStatus classStatus = new ClassStatus();
        classStatus.setId(ID_STATUS_CLASS_DEFAULT);
        classDTO.setClassStatus(classStatus);

        try {
            classService.create(classDTO);
            boolean continueAdding = "true".equals(isContinue);
            if (continueAdding){
                return "redirect:/class/add?create-success";
            }
            return "redirect:/class?create-success";
        }catch (Exception e){
            return "redirect:/class/add?create-error";
        }

    }
    @GetMapping("/edit/{id}")
    public String editClass(@PathVariable("id") Long id, Model model) {
        model.addAttribute("methods", methodService.getAll());
        model.addAttribute("courses", courseService.getAll());
        model.addAttribute("classStatus", classStatusService.getAll());
        ClassDTO classDTO = classService.getClassById(id);
        model.addAttribute("class", classDTO);
        return UPDATE_CLASS_VIEW;
    }

    @PostMapping("/update")
    public String updateClass( @ModelAttribute("class") ClassDTO classDTO) {
        try {
            classService.update(classDTO);
            return "redirect:/class?update-success";
        }catch (Exception e){
            return "redirect:/class/edit/"+classDTO.getId()+"?update-error";
        }
    }

}
