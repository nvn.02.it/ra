package com.ra.classroom.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Class {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String classCode;
    private Date startDate;
    private String classSchedule;
    private String startHour;
    private String endHour;
    private Integer numberOfStudents;

    @ManyToOne()
    @JoinColumn(name = "class_status_id")
    private ClassStatus classStatus;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "method_course_id")
    private MethodCourse methodCourse;
}
