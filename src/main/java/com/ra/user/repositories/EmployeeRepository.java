package com.ra.user.repositories;

import com.ra.user.entities.Employee;
import com.ra.user.entities.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee findFirstByUserInfoId(int userId);;
}
