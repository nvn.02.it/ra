package com.ra.user.repositories;

import com.ra.user.entities.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository interface for managing UserInfo entities.
 */
@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, Integer> {
    UserInfo findFirstByEmail(String email);
}
