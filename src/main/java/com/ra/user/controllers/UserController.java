package com.ra.user.controllers;

import com.ra.config.SecurityUtil;
import com.ra.jwt.services.JwtService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Controller class for handling user-related operations.
 */
@Controller
@AllArgsConstructor
public class UserController {
    private final JwtService jwtService;

    private final MessageSource messageSource;

    private final AuthenticationManager authenticationManager;

    private final SecurityUtil securityUtil;

    /**
     * Displays the login form.
     *
     * @return The name of the login view template.
     */
    @GetMapping("/login")
    public String showLogin(Model model) {
        // Lấy thông tin user
        securityUtil.getUserInfo(model);

        return "login";
    }

    /**
     * Authenticates the user and generates a JWT token.
     *
     * @param username The username of the user.
     * @param password The password of the user.
     * @param response The HTTP servlet response object.
     * @return The redirection URL after successful authentication.
     * @throws UsernameNotFoundException if the user with the given username is not found.
     */
    @PostMapping("/generateToken")
    public String authenticateAndGetToken(@RequestParam("username") String username,
                                          @RequestParam("password") String password,
                                          HttpServletResponse response) {
        Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (UsernameNotFoundException ex) {
            throw new UsernameNotFoundException(
                    messageSource.getMessage("validation.user.error", null, null));
        }

        if (authentication.isAuthenticated()) {
            String token = jwtService.generateToken(username);
            Cookie cookie = new Cookie("token", token);
            cookie.setMaxAge(Integer.MAX_VALUE);
            cookie.setPath("/");
            response.addCookie(cookie);
            System.out.println("token: " + token);
            return "redirect:/category/";
        } else {
            throw new UsernameNotFoundException(
                    messageSource.getMessage("validation.user.error", null, null));
        }
    }
}
