package com.ra.user.services;

import com.ra.user.entities.Role;
import com.ra.user.repositories.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service class for performing business logic operations related to roles.
 */
@Service
@AllArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;

    /**
     * Retrieves a role by its unique identifier.
     *
     * @param id the unique identifier of the role
     * @return the role if found, otherwise throws a RuntimeException
     */
    public Role geRoleById(Integer id) {
        return roleRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Default role not found"));
    }

    /**
     * Retrieves all roles.
     *
     * @return a list of all roles
     */
    public List<Role> getAll(){
        return roleRepository.findAll();
    }

    /**
     * Saves a new role with the given name.
     *
     * @param name the name of the role to be saved
     * @return the saved role
     */
    public Role saveRole(String name){
        Role role = new Role();
        role.setName(name);
        return roleRepository.save(role);
    }

}