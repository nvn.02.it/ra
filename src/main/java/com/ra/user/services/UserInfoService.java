package com.ra.user.services;

import com.ra.user.dtos.UserDTO;
import com.ra.user.entities.Role;
import com.ra.user.entities.UserInfoDetails;
import com.ra.user.entities.UserInfo;
import com.ra.user.repositories.UserInfoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service class for managing user information.
 */
@Service
public class UserInfoService implements UserDetailsService {

    @Value("${role.default}")
    private Integer roleDefault;

    @Autowired
    private UserInfoRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    RoleService roleService;

    @Autowired
    private  ModelMapper mapper;

    /**
     * Load user details by username.
     *
     * @param username The username of the user to load details for.
     * @return UserDetails object containing user details.
     * @throws UsernameNotFoundException if the user with the given username is not found.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // Try finding user by email
        UserInfo userInfo = repository.findFirstByEmail(username);

        // If user is not found, throw exception
        if (userInfo == null || userInfo.isBlock())
            throw new UsernameNotFoundException("User not found: " + username);

        return new UserInfoDetails(userInfo);
    }

    /**
     * Save a new user.
     *
     * @param userDto The UserDto object containing user information to save.
     */
    public void saveUser(UserDTO userDto) {
        UserInfo user = mapper.map(userDto, UserInfo.class);
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        Role role = roleService.geRoleById(roleDefault);
        user.setRole(role);
        repository.save(user);
    }

    /**
     * Find a user by their name.
     *
     * @param email The email of the user to find.
     * @return The UserInfo object representing the found user, or null if not found.
     */
    public UserInfo findFirstByEmail(String email) {
        return repository.findFirstByEmail(email);
    }
}
