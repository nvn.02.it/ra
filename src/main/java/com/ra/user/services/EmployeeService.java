package com.ra.user.services;

import com.ra.user.entities.Employee;
import com.ra.user.entities.UserInfo;
import com.ra.user.repositories.EmployeeRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    /**
     * Find an employee by their user info id.
     *
     * @param userId The id of the user info associated with the employee to find.
     * @return The Employee object representing the found employee, or null if not found.
     */
    public Employee findFirstByUserInfoId(int userId) {
        return employeeRepository.findFirstByUserInfoId(userId);
    }
}
