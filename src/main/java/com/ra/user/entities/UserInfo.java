package com.ra.user.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


/**
 * Represents a user entity in the mobile store system.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo { 
  
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    private int id;

    private String email;

    private String password;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    private String passwordForgotToken;

    private String tokenActive;

    private Date date;

    private boolean block;
} 