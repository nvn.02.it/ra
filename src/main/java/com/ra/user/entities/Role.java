package com.ra.user.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

/**
 * Represents a role entity in the application.
 */
@Getter
@Setter
@Entity
@Table(name = "roles")
@AllArgsConstructor
@NoArgsConstructor
public class Role {
    @Id
    private Integer id;

    private String name;

    @OneToMany(mappedBy = "role")
    private List<UserInfo> users;
}