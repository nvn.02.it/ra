package com.ra.course.services;


import com.ra.course.dtos.CourseDTO;
import com.ra.course.entities.Course;
import com.ra.course.repositories.CourseRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CourseService {

    private  final CourseRepository courseRepository;
    private final ModelMapper modelMapper;

    /**
     * Retrieves all courses.
     *
     * @return a list of CourseDTO objects.
     */
    public List<CourseDTO> getAll(){
        List<Course> courses = courseRepository.findAll();
        List<CourseDTO> courseDTOS = courses.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
        return courseDTOS;
    }
    /**
     * Converts a Course entity to a CourseDTO.
     *
     * @param course the Course entity to convert.
     * @return the converted CourseDTO object.
     */
    private CourseDTO convertToDTO(Course course) {
        CourseDTO courseDTO = modelMapper.map(course, CourseDTO.class);
        return courseDTO;
    }
}
