package com.ra.course.dtos;

import com.ra.classroom.entities.MethodCourse;
import lombok.Getter;
import lombok.Setter;


import java.util.List;

@Getter
@Setter
public class CourseDTO {
    public Long id;
    public String name;
    public Integer totalHours;
    public List<MethodCourse> methodCourse;
}
