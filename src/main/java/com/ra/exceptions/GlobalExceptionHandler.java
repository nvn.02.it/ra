package com.ra.exceptions;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NoHandlerFoundException.class)
    public void handleNoHandlerFoundException(NoHandlerFoundException ex) {
    }

    @ExceptionHandler(ClassAlreadyExistsException.class)
    public String handleClassAlreadyExistsException(ClassAlreadyExistsException e) {
        return e.getMessage();
    }

}