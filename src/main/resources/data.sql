USE ra;

-- Thêm role nếu chưa có
INSERT INTO roles (id, name) VALUES (1, 'ROLE_ADMIN') ON DUPLICATE KEY UPDATE name = VALUES(name);
INSERT INTO roles (id, name) VALUES (2, 'ROLE_SALER') ON DUPLICATE KEY UPDATE name = VALUES(name);

-- Thêm người dùng admin nếu chưa có
-- password là Abc@12345 đã mã hoá
--INSERT INTO user_info (id,name, password, role_id)
--VALUES (1,'admin', '$2a$10$Xp8Vg6ndwE2lWIOccxgJUeFmaFHULGDRNCbyLUxOjR1SWKRhC0nZm', 1)
--ON DUPLICATE KEY UPDATE name = VALUES(name), password = VALUES(password), role_id = VALUES(role_id);

--thêm khoá học
INSERT INTO `ra`.`course` (`id`, `name`)
SELECT '2', 'lập trình di động'
WHERE NOT EXISTS (SELECT 1 FROM `ra`.`course` WHERE `id` = '2');

INSERT INTO `ra`.`course` (`id`, `name`)
SELECT '3', 'lập trình c'
WHERE NOT EXISTS (SELECT 1 FROM `ra`.`course` WHERE `id` = '3');

INSERT INTO `ra`.`course` (`id`, `name`)
SELECT '4', 'lập trình hướng đối tượng'
WHERE NOT EXISTS (SELECT 1 FROM `ra`.`course` WHERE `id` = '4');

--thêm phương thức học
INSERT INTO `ra`.`method` (`id`, `name`)
SELECT '2', 'offline'
WHERE NOT EXISTS (SELECT 1 FROM `ra`.`method` WHERE `id` = '2');

INSERT INTO `ra`.`method` (`id`, `name`)
SELECT '3', 'online or offline'
WHERE NOT EXISTS (SELECT 1 FROM `ra`.`method` WHERE `id` = '3');

-- trạng thái

INSERT INTO `ra`.`class_status` (`id`, `name`) VALUES ('1', 'Tạo mới')
ON DUPLICATE KEY UPDATE id = VALUES(id), name = VALUES(name);

-- Thêm hoặc cập nhật thông tin nhân viên nếu chưa có
INSERT INTO employee (
    id, full_name, gender, birth_date, phone_number, location, basic_salary, level, user_id, status
) VALUES (
    1, 'Admin', true, '2002-01-01', '1234567890', "Viet Nam", 50000.00, null, 1, true
) ON DUPLICATE KEY UPDATE
    full_name = VALUES(full_name),
    gender = VALUES(gender),
    birth_date = VALUES(birth_date),
    phone_number = VALUES(phone_number),
    location = VALUES(location),
    basic_salary = VALUES(basic_salary),
    level = VALUES(level),
    user_id = VALUES(user_id),
    status = VALUES(status);
