$(document).ready(function() {
    // Thêm sự kiện click vào header-avatar để hiển thị tùy chọn
    $(".header-avatar").click(function () {
        $(".header-avatar-options").toggleClass("show");
    });

    // Ngăn sự kiện click của tùy chọn lan truyền đến header-avatar
    $(".header-avatar-options").click(function (event) {
        event.stopPropagation();
    });

    // Đóng tùy chọn khi click bên ngoài header-avatar-options
    $(document).click(function (event) {
        if (!$(event.target).closest(".header-avatar").length) {
          $(".header-avatar-options").removeClass("show");
        }
    });

    /*ALERT*/
    $(document).on('click', '.toggle-password', function() {
        $(this).toggleClass('bi-eye-slash-fill');
        var input = $("#password");
        if (input.attr("type") === "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    function createAlert(message, type) {
        const alertIcon = type === 'error' ?
            `<svg width="35" height="35" viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                <mask id="mask0_2278_2909" style="mask-type: alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="35" height="35">
                    <rect width="35" height="35" fill="#D9D9D9" />
                </mask>
                <g mask="url(#mask0_2278_2909)">
                    <path d="M17.5001 32.0834C15.4827 32.0834 13.5869 31.7005 11.8126 30.9349C10.0383 30.1693 8.49487 29.1302 7.18237 27.8177C5.86987 26.5052 4.83081 24.9618 4.06519 23.1875C3.29956 21.4132 2.91675 19.5174 2.91675 17.5C2.91675 15.4827 3.29956 13.5868 4.06519 11.8125C4.83081 10.0382 5.86987 8.49481 7.18237 7.18231C8.49487 5.86981 10.0383 4.83075 11.8126 4.06512C13.5869 3.2995 15.4827 2.91669 17.5001 2.91669C19.5174 2.91669 21.4133 3.2995 23.1876 4.06512C24.9619 4.83075 26.5053 5.86981 27.8178 7.18231C29.1303 8.49481 30.1693 10.0382 30.935 11.8125C31.7006 13.5868 32.0834 15.4827 32.0834 17.5C32.0834 19.5174 31.7006 21.4132 30.935 23.1875C30.1693 24.9618 29.1303 26.5052 27.8178 27.8177C26.5053 29.1302 24.9619 30.1693 23.1876 30.9349C21.4133 31.7005 19.5174 32.0834 17.5001 32.0834Z" fill="#EF665B" />
                    <path d="M17.5001 32.0834C15.4827 32.0834 13.5869 31.7005 11.8126 30.9349C10.0383 30.1693 8.49487 29.1302 7.18237 27.8177C5.86987 26.5052 4.83081 24.9618 4.06519 23.1875C3.29956 21.4132 2.91675 19.5174 2.91675 17.5C2.91675 15.4827 3.29956 13.5868 4.06519 11.8125C4.83081 10.0382 5.86987 8.49481 7.18237 7.18231C8.49487 5.86981 10.0383 4.83075 11.8126 4.06512C13.5869 3.2995 15.4827 2.91669 17.5001 2.91669C19.5174 2.91669 21.4133 3.2995 23.1876 4.06512C24.9619 4.83075 26.5053 5.86981 27.8178 7.18231C29.1303 8.49481 30.1693 10.0382 30.935 11.8125C31.7006 13.5868 32.0834 15.4827 32.0834 17.5C32.0834 19.5174 31.7006 21.4132 30.935 23.1875C30.1693 24.9618 29.1303 26.5052 27.8178 27.8177C26.5053 29.1302 24.9619 30.1693 23.1876 30.9349C21.4133 31.7005 19.5174 32.0834 17.5001 32.0834ZM17.5001 29.1667C20.757 29.1667 23.5157 28.0365 25.7761 25.7761C28.0365 23.5156 29.1667 20.757 29.1667 17.5C29.1667 14.2431 28.0365 11.4844 25.7761 9.22398C23.5157 6.96356 20.757 5.83335 17.5001 5.83335C14.2431 5.83335 11.4845 6.96356 9.22404 9.22398C6.96362 11.4844 5.83341 14.2431 5.83341 17.5C5.83341 20.757 6.96362 23.5156 9.22404 25.7761C11.4845 28.0365 14.2431 29.1667 17.5001 29.1667Z" fill="#EF665B" />
                </g>
                <mask id="mask1_2278_2909" style="mask-type: alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="35" height="35">
                    <rect width="35" height="35" fill="#D9D9D9" />
                </mask>
                <g mask="url(#mask1_2278_2909)">
                    <path d="M12.2499 24.7916L17.4999 19.5416L22.7499 24.7916L24.7916 22.75L19.5416 17.5L24.7916 12.25L22.7499 10.2083L17.4999 15.4583L12.2499 10.2083L10.2083 12.25L15.4583 17.5L10.2083 22.75L12.2499 24.7916Z" fill="#1C1B1F"/>
                    <path d="M12.2499 24.7916L17.4999 19.5416L22.7499 24.7916L24.7916 22.75L19.5416 17.5L24.7916 12.25L22.7499 10.2083L17.4999 15.4583L12.2499 10.2083L10.2083 12.25L15.4583 17.5L10.2083 22.75L12.2499 24.7916Z" fill="white"/>
                </g>
            </svg>`
            :
            `<svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
              <circle cx="14" cy="14" r="14" fill="#00B074" />
              <path d="M12.4216 16.8408L20.1629 9.66626L21.3545 10.7698L12.4216 19.0479L7.06201 14.0812L8.25285 12.9777L12.4216 16.8408Z" fill="white"/>
            </svg>`;

        const closeIcon = type === 'error' ?
            `<svg class="close-alert" width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M8.6579 6.2225L15.3921 0L17.3158 1.7775L10.5816 8L17.3158 14.2225L15.3921 16L8.6579 9.7775L1.92367 16L0 14.2225L6.73422 8L0 1.7775L1.92367 0L8.6579 6.2225Z" fill="#EF665B"/>
            </svg>`
            :
            `<svg class="close-alert" width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M8.6579 6.2225L15.3921 0L17.3158 1.7775L10.5816 8L17.3158 14.2225L15.3921 16L8.6579 9.7775L1.92367 16L0 14.2225L6.73422 8L0 1.7775L1.92367 0L8.6579 6.2225Z" fill="#00B074"/>
            </svg>`;

        const alert = $("<div>").addClass(type == 'error' ? "alert-type-error text-danger" : "alert-type-success text-success").addClass("alert alert-success fade show d-flex align-items-center").attr("role", "alert").html(
            alertIcon +
            `<p class="mx-2 mb-0">${message}</p>`
            +  closeIcon
        );
      return alert;
    }

    function showAlert(message, type) {
        const alert = createAlert(message, type);
        $("#alert-container").append(alert);
        animateAlert(alert);
    }

    function closeAlert(alert) {
        alert.addClass("slide-out");
        setTimeout(() => {
            alert.remove();
        }, 500);
    }

    function animateAlert(alert) {
        let currentWidth = 100;
        const interval = setInterval(() => {
            if (currentWidth <= 0) {
                clearInterval(interval);
                closeAlert(alert);
            } else {
                currentWidth -= 1.3;
                alert.css("--after-width", `${currentWidth}%`);
            }
        }, 50);
    }

    $(document).on("click", function (event) {
        const target = $(event.target);
        if (
            target.hasClass("close-alert") ||
            target.closest(".close-alert").length > 0
        ) {
            const alert = target.closest(".alert");
            if (alert.length > 0) {
                closeAlert(alert);
            }
        }
    });

    /*LOGIN FORM*/
    $("#btn-submit").click(function(event) {
        event.preventDefault(); // Ngăn chặn gửi form mặc định

        // Lấy giá trị của email và mật khẩu từ form
        var email = $("#email").val();
        var password = $("#password").val();
        var rememberMe = $("#remember").prop("checked");

        // Reset alert container
        $("#alert-container").empty();

        // Kiểm tra điều kiện cho email
        if (email.trim() === "") {
            showAlert("Vui lòng nhập địa chỉ email", 'error');
            return;
        } else if (email.length < 6) {
            showAlert("Vui lòng nhập tối thiểu 6 ký tự", 'error');
            return;
        } else if (email.length > 256) {
            showAlert("Vui lòng nhập tối đa 256 ký tự", 'error');
            return;
        } else if (!isValidEmail(email)) {
            showAlert("Địa chỉ email không hợp lệ", 'error');
            return;
        }

        // Kiểm tra điều kiện cho mật khẩu
        if (password.trim() === "") {
            showAlert("Vui lòng nhập mật khẩu", 'error');
            return;
        }

        // Kiểm tra tính đúng sai
        $.ajax({
            type: "POST",
            url: "/login", // Đường dẫn đến endpoint xác thực
            data: {
                username: email,
                password: password,
                remember: rememberMe ? "on" : "off"
            },
            success: function(data) {
                // Xử lý kết quả thành công
                showAlert("Đăng nhập thành công", 'success');
                setTimeout(function() {
                    window.location.href = "/user-management";
                }, 2000);
            },
            error: function(xhr, textStatus, errorThrown) {
                // Xử lý lỗi
                var code = xhr.status;
                showAlert("Địa chỉ email hoặc mật khẩu không đúng", 'error');
            }
        });
    });

    function isValidEmail(email) {
//        var emailRegex = /^[a-zA-Z]{1,}[-.]?[a-zA-Z]{1,}@[a-zA-Z0-9]{2,}\.[a-zA-Z]+$/;
        var emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,}$/;

        // Kiểm tra tất cả các điều kiện trong một câu lệnh if
        if (
            !emailRegex.test(email) ||                     		// Không phù hợp với biểu thức chính quy
            email.includes("..") ||                        		// Chứa ".."
            email.startsWith(".") || email.endsWith(".") ||    	// Bắt đầu hoặc kết thúc bằng "."
            email.endsWith("@") ||                             	// Kết thúc bằng "@"
            email.includes("-@") || email.includes("@-")        // Ký tự '-' trước hoặc sau '@'
        ) {
            return false;
        }

        // Nếu không có vấn đề gì, email được coi là hợp lệ
        return true;
    }
});