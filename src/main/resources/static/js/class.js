var classSchedule = $("#dataClassSchedule").val();
var selectedValues = classSchedule.split(","); // Chuyển chuỗi thành mảng các giá trị đã chọn

$("#classSchedule option").each(function () {
    if (selectedValues.includes($(this).val())) { // Kiểm tra xem giá trị của option có trong chuỗi classSchedule không
        $(this).prop("selected", true); // Nếu có, đánh dấu là đã chọn
    }
});

$('#classSchedule').select2({
    placeholder: "Chọn mục",
    allowClear: true,
    tags: true
});

$(".btn-submit").click(function (e) {

    let valid = true;
    $(".error").text(""); // Clear previous error messages

    // Tên khóa học
    if ($("#methodCourse_course_id").val() === "") {
        $("#courseError").text("* Vui lòng chọn khóa học");
        valid = false;
    }

    // Mã lớp
    let classCode = $("#classCode").val();
    if (classCode.length < 6) {
        $("#classCodeError").text("* Vui lòng nhập ít nhất 6 kí tự");
        valid = false;
    } else if (classCode.length > 50) {
        $("#classCodeError").text("* Vui lòng nhập tối đa 50 kí tự");
        valid = false;
    }

    // Trạng thái
    if ($("#classStatus").val() === "") {
        $("#classStatusError").text("* Vui lòng chọn trạng thái");
        valid = false;
    }

    // Hình thức học
    if ($("#methodCourse_method_id").val() === "") {
        $("#methodError").text("* Vui lòng chọn hình thức học");
        valid = false;
    }
    // ngày bắt đầu lớp
    let startDate = $("#datepicker").val();
    if (startDate === "") {
        $("#startDateError").text("* Vui lòng chọn ngày bắt đầu lớp");
        valid = false;
    } else {
        let today = new Date().toISOString().split('T')[0];
        let startDateObj = new Date(startDate.replace(/-/g, '/'));
        let todayObj = new Date(today.replace(/-/g, '/'));
        if (startDateObj < todayObj) {
            $("#startDateError").text("* Vui lòng chọn ngày bắt đầu lớp từ ngày hiện tại trở đi");
            valid = false;
        }
    }

    // Ca học
    if ($("#classSchedule").val() === "") {
        $("#classScheduleError").text("* Vui lòng chọn ca học");
        valid = false;
    }

    // Giờ vào học
    let startHour = $("#startHour").val();
    if (startHour === "") {
        $("#startHourError").text("* Vui lòng nhập/ chọn giờ vào học");
        valid = false;
    }

    // Giờ tan học
    let endHour = $("#endHour").val();
    if (endHour === "") {
        $("#endHourError").text("* Vui lòng nhập/ chọn giờ tan học");
        valid = false;
    }

    // So sánh giờ vào học và giờ tan học
    if (startHour !== "" && endHour !== "" && startHour >= endHour) {
        $("#startHourError").text("* Vui lòng nhập/ chọn giờ vào học trước giờ tan học");
        $("#endHourError").text("* Vui lòng nhập/ chọn giờ tan học sau giờ vào học");
        valid = false;
    }

    if(valid){
        $("#form-class").submit();
    }else{
        $(".model-confirm").modal('hide');
    }
  

});

